var seriesApp = angular.module('seriesApp',['ngRoute','ngResource','seriesServices', 'seriesControllers']);

seriesApp.config(function($routeProvider){
	$routeProvider
	.when('/series',{
		templateUrl : 'assets/list.html',
	})
	.when('/serie/detalhes/:id',{
		templateUrl : 'assets/view.html',
	})
	.when('/serie/new',{
		templateUrl : 'assets/add.html',
	})
	.when('/serie/edit/:id',{
		templateUrl : 'assets/edit.html',
	})
	.otherwise({redirectTo : '/series'});
});