var seriesServices = angular.module('seriesServices',['ngResource']);

seriesServices.factory('Serie', function($resource){
	return $resource('/SeriesCrudAngular/series/:id',{id : '@id'}, {
		update : {
			method : 'PUT'
		}
	});
});