var seriesControllers = angular.module('seriesControllers',['seriesServices']);

seriesControllers.controller('listCtrl', function($scope,$http,$location,Serie){
	$scope.series = Serie.query();

	$scope.ordenar = function(order){
		$scope.order = order;
	};

	$scope.apagarSerie = function(serie){
		swal({
          title: "Deseja realmente apagar?",
	      text: "",
          type: "warning",
          showCancelButton: true,
          confirmButtonClass: 'btn-danger',
          confirmButtonText: 'Sim, por favor!',
          cancelButtonText: "Não, erro meu!",
          closeOnConfirm: false,
          closeOnCancel: false
        },
        function(isConfirm){
          if (isConfirm){
            swal("Apagado!", "", "success");
            serie.$delete(function(){
				$location.path('#/series');
			});
          } else {
            swal("Cuidado então", "", "error");
          }
        });
	};
	
});

seriesControllers.controller('addCtrl', function($scope,$http,$location,Serie){
	$scope.serie = new Serie();

	$scope.addSerie = function(){
		$scope.serie.$save(function(){
			    swal("Salvo com Sucesso!", "", "success");
				$location.path('#/series');
		});
	};
});

seriesControllers.controller('EditCtrl',function($scope, $http, $location,$routeParams,Serie){
	$scope.serie = Serie.get({ id: $routeParams.id});

	$scope.atualizarSerie = function(){
		$scope.serie.$update(function(){
			swal("Serie atualizada com sucesso!", "", "success");
			$location.path('#/series');
		});
	};
});

seriesControllers.controller('ViewCtrl',function($scope, $http, $location,$routeParams,Serie){
	$scope.serie = Serie.get({ id: $routeParams.id});
});