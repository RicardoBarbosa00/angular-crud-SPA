<!DOCTYPE html>
<html ng-app="seriesApp">
	<head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
		<title>Series</title>
		<asset:stylesheet href="bower_components/bootstrap/dist/css/bootstrap.min.css"/>
        <asset:stylesheet href="bower_components/bootstrap-sweetalert/lib/sweet-alert.css"/>
		<asset:javascript src="app.js"/>
	</head>
	<body>
		   <!-- Navigation -->
    <nav class="navbar navbar-default">
        <div class="container-fluid">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header page-scroll">
                <a class="navbar-brand" href="#/series">Series</a>
            </div>

            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                <ul class="nav navbar-nav navbar-right">
                    <li class="page-scroll">
                        <a href="#/series">Listar</a>
                    </li>
                    <li class="page-scroll">
                        <a href="#/serie/new">Cadastrar</a>
                    </li>
                    <li class="page-scroll">
                        <div class="navbar-form navbar-left" role="search">
                            <div class="form-group">
                                <input type="text" ng-model="searchSerie" class="form-control" placeholder="Buscar...">
                            </div>
                        </div>
                    </li>
                </ul>
            </div>
        </div>
    </nav>
    
    <div class="container">
		<div ng-view></div>
	</div>
	</body>
</html>
